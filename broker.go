package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"log"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

// TLS option
func TLSfromFile(absCAfile, absCertfile, absKeyfile string) *tls.Config {
	cert := x509.NewCertPool()
	ca, err := ioutil.ReadFile(absCAfile)
	if err != nil {
		log.Fatalln(err.Error())
	}
	cert.AppendCertsFromPEM(ca)
	keypair, err := tls.LoadX509KeyPair(absCertfile, absKeyfile)
	if err != nil {
		panic(err)
	}
	return &tls.Config{
		RootCAs:            cert,
		ClientAuth:         tls.NoClientCert,
		ClientCAs:          nil,
		InsecureSkipVerify: true,
		Certificates:       []tls.Certificate{keypair},
	}
}

func TLSfileClientnotset(absCAfile string) *tls.Config {
	cert := x509.NewCertPool()
	ca, err := ioutil.ReadFile(absCAfile)
	if err != nil {
		log.Fatalln(err.Error())
	}
	cert.AppendCertsFromPEM(ca)
	return &tls.Config{
		RootCAs: cert,
	}
}

// if using CA
func setTLS(fileCA, filePem, fileKey string) *tls.Config {
	if filePem != "" {
		return TLSfromFile(fileCA, filePem, fileKey)
	} else {
		return TLSfileClientnotset(fileCA)
	}
}

func InitBroker(broker, user, pass string, port int) mqtt.Client {
	option := mqtt.NewClientOptions()
	option.AddBroker(fmt.Sprintf("tcp://%s:%d", broker, port))
	option.SetClientID(time.Now().GoString())
	//optional
	option.SetUsername(user)
	option.SetPassword(pass)
	return mqtt.NewClient(option)
}
